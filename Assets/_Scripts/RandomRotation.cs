﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{
	public int spectrumIndex = 0;
	float alpha = 1;
	[SerializeField] private Vector3 rotationSpeed;
	[SerializeField] private int speedLimit = 10;
	private MeshRenderer renderer;
	private Rigidbody rb;
	
	void Start ()
	{
//		Transform imageTarget = GameObject.Find("ImageTarget").GetComponent<Transform>();
//		float offset = Random.Range(-3, 3);
//		Vector3 pieceTargetPos = imageTarget.position + new Vector3(offset, offset, offset);
//		Vector3 direction = imageTarget.position - transform.position;
//		rb = GetComponent<Rigidbody>();
//		rb.AddForce(-imageTarget.up*Random.Range(5,15), ForceMode.Impulse);
		renderer = GetComponentInChildren<MeshRenderer>();
		renderer.material.color = Random.ColorHSV();
		//transform.LookAt(GameObject.Find("StartLookAt").transform);
		//rotationSpeed = new Vector3(Random.Range(-speedLimit, speedLimit),
		//	Random.Range(-speedLimit, speedLimit), Random.Range(-speedLimit, speedLimit));	
	}
	
	void Update () {
		//transform.Rotate(rotationSpeed * Time.deltaTime /*(spectrum[spectrumIndex]*100)*/);
		Color myColor = renderer.material.color;
		myColor.a = (alpha -= (Time.deltaTime * 0.1f));
		renderer.material.color = myColor;
		if (alpha <= 0)
		{
			Destroy(gameObject);
		}

		//GetComponent<Renderer>().material.color = new Color(1,1,1,alpha);
	}
}
