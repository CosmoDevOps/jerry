﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA;
using Vuforia;
using UnityEngine.Events;

public class FiguresController : MonoBehaviour, ITrackableEventHandler
{
	public Transform instantiationPoint;
	
	[SerializeField] private GameObject[] figuresPrefabs;
	[SerializeField] private int figuresToSpawn;
	[SerializeField] private Vector3 xyzLimits;
	List<GameObject> clones = new List<GameObject>();
	[SerializeField] private AudioClip[] clips;
	[SerializeField] private AudioSource audioSource;
	[SerializeField] private Transform floor;
	[SerializeField] private ImageTargetBehaviour[] targets;
	private ImageTargetBehaviour currentTarget = null;
	[SerializeField] Vector3 refPos = new Vector3(0,0,0);
	[SerializeField] private MeshRenderer[] floors;
	
//	public UnityAction OnTrackingFound;
//	public UnityAction OnTrackingLost;
//	private TrackableBehaviour trackableBehaviour = null;
//
//	private List<TrackableBehaviour.Status> trackingFound = new List<TrackableBehaviour.Status>()
//	{
//		TrackableBehaviour.Status.DETECTED,
//		TrackableBehaviour.Status.TRACKED,
//		TrackableBehaviour.Status.EXTENDED_TRACKED
//	};
//	
//	private List<TrackableBehaviour.Status> trackingLost = new List<TrackableBehaviour.Status>()
//	{
//		TrackableBehaviour.Status.NO_POSE,
//		TrackableBehaviour.Status.TRACKED,
//		
//	};
//
//	private void OnEnable()
//	{
//		throw new System.NotImplementedException();
//	}

	void Start () {
		
		UpdateInstantiationPoint();
		
//		for (int i = 0; i < figuresToSpawn; i++)
//		{
//			Vector3 pos = new Vector3(Random.Range(0, xyzLimits.x), Random.Range(0, xyzLimits.y),
//				Random.Range(0, xyzLimits.z));
//			clones.Add(Instantiate(figuresPrefabs[Random.Range(0, figuresPrefabs.Length)], pos, Quaternion.identity));
//			//clone.GetComponent<RandomRotation>().spectrumIndex = i;
//		}
		audioSource = GetComponent<AudioSource>();
		//StartCoroutine(PlayAudioClips());
		StartCoroutine(ResetCurrentTarget());
	}

	IEnumerator ResetCurrentTarget()
	{
		while (true)
		{
			yield return new WaitForSeconds(2);
			instantiationPoint = null;
		}
		
	}

	void Update () {
		float[] spectrum = new float[256];
		AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
		for (int i = 1; i < spectrum.Length - 1; i++)
		{
			//Debug.DrawLine(new Vector3(i - 1, Mathf.Log(spectrum[i - 1]) + 10, 2), new Vector3(i, Mathf.Log(spectrum[i]) + 10, 2), Color.cyan);
			if (Mathf.Log(spectrum[i - 1]) + 10 > 8)
			{
				if (instantiationPoint != null)
				{

					Vector3 pos = instantiationPoint.position + new Vector3(Random.Range(-xyzLimits.x, xyzLimits.x), Random.Range(-xyzLimits.y, xyzLimits.y),
						              Random.Range(-xyzLimits.z, xyzLimits.z));
					//print(pos);
					Instantiate(figuresPrefabs[Random.Range(0, figuresPrefabs.Length)], pos, Random.rotation);
				}
				else
				{
					UpdateInstantiationPoint();
				}

//				
//				foreach (var t in targets)
//				{
//					if (t.isActiveAndEnabled)
//					{
//						refPos = t.transform.position;
//					}
//				}
//				Vector3 pos = refPos + new Vector3(Random.Range(-xyzLimits.x, xyzLimits.x), Random.Range(-xyzLimits.y, xyzLimits.y),
//					              Random.Range(-xyzLimits.z, xyzLimits.z));
//				Instantiate(figuresPrefabs[Random.Range(0, figuresPrefabs.Length)], pos, Random.rotation);
			}
		}
	}

	void UpdateInstantiationPoint()
	{
		foreach (var t in targets)
		{
			if (t.CurrentStatus == TrackableBehaviour.Status.TRACKED)
			{
				//currentTarget = t;
				instantiationPoint = t.transform;
			}
		}
	}

	IEnumerator PlayAudioClips()
	{
		int currentClip = Random.Range(0, clips.Length-1);

		while (true)
		{
			audioSource.clip = clips[currentClip];
			audioSource.Play();
			yield return  new WaitForSeconds(clips[currentClip].length);
			if (currentClip < clips.Length-1)
			{
				currentClip++;
			}
			else
			{
				currentClip = 0;
			}
		}
	}

	public void UpdateZOffset(int amount)
	{
		Vector3 floorPos = floor.localPosition;
		floorPos.y += amount;
		floor.localPosition = floorPos;
	}

	public void DestroyCanvas()
	{
		foreach (var f in floors)
		{
			f.material.color = new Color(1,1,1,0);
		}
		GameObject.Find("Canvas").SetActive(false);
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		
	}
}
