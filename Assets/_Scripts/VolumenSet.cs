﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumenSet : MonoBehaviour
{
	private AudioSource source;
	[SerializeField]private BoxCollider collider;
	
	void Start ()
	{
		source = GetComponent<AudioSource>();
	}
	
	void Update () {
		if (collider.enabled)
		{
			source.volume = 1;
		}
		else
		{
			source.volume = 0;
		}
	}
}
